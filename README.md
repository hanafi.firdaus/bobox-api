### Bobobox-API Management Hotel


## Instalasi
1. Install Go versi 1.13.8
2. Aktifkan go mod dengan cara set do environment GO111MODULE=ON
3. Install postgresql versi(PostgreSQL 12.3 (Ubuntu 12.3-1.pgdg18.04+1))
4. Import bak file db (file bak dilampirkan)

## Menjalankan project
1. Run di terminal linux ```go run .```
2. Link postman (https://www.getpostman.com/collections/33cf0ec7602494e2015a)
3. Env postman dilampirkan di email

## API
1. User
    - http://localhost:10000/api/v1/user/create (Isian role di body bisa admin atau customer, jika admin hanya bisa akses api management, customer hanya bisa akses api reservasi)
    - http://localhost:10000/api/v1/user/login

2. Reservasi
    - http://localhost:10000/api/v1/reservation/check?checkin_date=2020-09-03&checkout_date=2020-09-03
    - http://localhost:10000/api/v1/reservation/order

3. Management
    - http://localhost:10000/api/v1/management/hotel
    - http://localhost:10000/api/v1/management/price
    - http://localhost:10000/api/v1/management/hotel?id=6
    - http://localhost:10000/api/v1/management/room
    - http://localhost:10000/api/v1/masterdata/room-status
    - http://localhost:10000/api/v1/reservation/checkin
    - http://localhost:10000/api/v1/management/price