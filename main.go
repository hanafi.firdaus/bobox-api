package main

import (
	config "bobobox-api/config"
	"bobobox-api/helper"
	"bobobox-api/service/handler"
	"bobobox-api/service/repository"
	"bobobox-api/service/usecase"
	"fmt"

	"github.com/joho/godotenv"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		panic(fmt.Sprintf("%s: %s", "Failed to load env", err))
	}

	e := echo.New()
	db := config.Postgresql()

	RegistrationRepo := repository.CreateRegistrationRepository(db)
	RegistrationUsecase := usecase.CreateRegistrationsUsecase(RegistrationRepo)
	RegistrationHandler := handler.CreateRegistrationHandler(RegistrationUsecase)

	ReservationRepo := repository.CreateReservationRepository(db)
	ReservationUsecase := usecase.CreateReservationUsecase(ReservationRepo)
	ReservationHandler := handler.CreateReservationHandler(ReservationUsecase)

	ManageRepo := repository.CreateManageHotelRepository(db)
	ManageUsecase := usecase.CreateManageHotelUsecase(ManageRepo)
	ManageHandler := handler.CreateManageHotelHandler(ManageUsecase)

	e.POST("/api/v1/user/create", RegistrationHandler.CreateUser)
	e.POST("/api/v1/user/login", RegistrationHandler.Login)

	r := e.Group("/api/v1")
	r.Use(middleware.JWT([]byte(helper.Getenv("JWT_SECRET"))))
	r.GET("/reservation/check", ReservationHandler.CheckAvailabllityRoomHotel)
	r.POST("/reservation/order", ReservationHandler.BookingRoomHotel)
	r.POST("/reservation/checkin", ReservationHandler.CheckinRoomHotel)

	r.POST("/management/hotel", ManageHandler.InsertHotelAndRoom)
	r.POST("/management/price", ManageHandler.InsertPriceBookingRoomHotel)
	r.PUT("/management/price", ManageHandler.UpdatePriceBookingRoomHotel)
	r.DELETE("/management/room", ManageHandler.DeleteRoomHotel)
	r.DELETE("/management/hotel", ManageHandler.DeleteHotel)

	// http://localhost:10000/api/v1/masterdata/room-status
	r.GET("/masterdata/:name", ManageHandler.GetMasterData)

	e.Logger.Fatal(e.Start(":10000"))
}
