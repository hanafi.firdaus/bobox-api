package model

import (
	"github.com/jinzhu/gorm"
)

type Registration struct {
	Nama        string `json:"nama"`
	Alamat      string `json:"alamat"`
	TempatLahir string `json:"tempat_lahir"`
}

// Reservation is
type Reservation struct {
	gorm.Model
	OrderID      string `gorm:"type:varchar(15)" json:"order_id"`
	RoomID       int    `gorm:"type:int" json:"room_id"`
	CustomerName string `gorm:"type:varchar(350)" json:"customer_name"`
	CheckinDate  string `gorm:"type:timestamp without time zone" json:"checkin_date"`
	CheckoutDate string `gorm:"type:timestamp without time zone" json:"checkout_date"`
	HotelID      int    `gorm:"type:int" json:"hotel_id"`
}

// Stay is
type Stay struct {
	gorm.Model
	ReservationID int      `gorm:"type:int" json:"reservation_id"`
	GuestName     string   `gorm:"type:varchar(350)" json:"guest_name"`
	RoomID        int      `gorm:"type:int" json:"room_id"`
	StayRoom      StayRoom `gorm:"foreignkey:stay_id"`
}

// StayRoom is
type StayRoom struct {
	gorm.Model
	StayID int    `gorm:"type:int" json:"stay_id"`
	RoomID int    `gorm:"type:int" json:"room_id"`
	Date   string `gorm:"type:date" json:"date"`
}

// Hotel is
type Hotel struct {
	gorm.Model
	HotelName string `gorm:"type:varchar(200)" json:"hotel_name"`
	Address   string `gorm:"type:varchar(350)" json:"address"`
	Room      []Room `gorm:"foreignkey:hotel_id"`
}

// Room is
type Room struct {
	gorm.Model
	HotelID    int    `gorm:"type:int" json:"hotel_id"`
	RoomNumber int    `gorm:"type:int" json:"room_number"`
	RoomStatus string `gorm:"type:varchar(100)" json:"room_status"`
}

// PricingRoom is
type PricingRoom struct {
	gorm.Model
	HotelID    int     `gorm:"type:int" json:"hotel_id"`
	RoomNumber int     `gorm:"type:int" json:"room_number"`
	Price      float64 `gorm:"type:numeric" json:"price"`
	Date       string  `json:"date"`
}

// ChekingAvaibility is
type ChekingAvaibility struct {
	CheckinDate  string `json:"checkin_date"`
	CheckoutDate string `json:"checkout_date"`
}

// AvaibiltyHotel is
type AvaibiltyHotel struct {
	Hotel      string `json:"hotel"`
	Address    string `json:"address"`
	Status     string `json:"status"`
	RoomNumber int    `json:"room_number"`
	Date       string `json:"date"`
	Price      string `json:"price"`
}

// User is
type User struct {
	gorm.Model
	Phone    string `gorm:"type:varchar(15)" json:"phone"`
	Name     string `gorm:"type:varchar(225)" json:"name"`
	Role     string `gorm:"type:varchar(15)" json:"role"`
	Password string `gorm:"type:varchar(225)" json:"password"`
}

// DeleteHotel is
type DeleteHotel struct {
	IDHotel int   `json:"id_hotel"`
	Room    []int `json:"room"`
}

// MasterData is
type MasterData struct {
	gorm.Model
	Kategori string `gorm:"type:varchar(225)" json:"kategori"`
	Value    string `gorm:"type:varchar(225)" json:"value"`
	Text     string `gorm:"type:varchar(225)" json:"text"`
	IsActive bool   `gorm:"type:boolean" json:"is_active"`
}

// Error is
type Error struct {
	Code    int         `json:"code"`
	Status  string      `json:"status"`
	Message interface{} `json:"message"`
}

// TableName is
func (Reservation) TableName() string {
	return "reservation"
}

// TableName is
func (Stay) TableName() string {
	return "stay"
}

// TableName is
func (StayRoom) TableName() string {
	return "stay_room"
}

// TableName is
func (Hotel) TableName() string {
	return "hotel"
}

// TableName is
func (Room) TableName() string {
	return "room"
}

// TableName is
func (PricingRoom) TableName() string {
	return "pricing_room"
}
