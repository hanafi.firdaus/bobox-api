package usecase

import (
	"bobobox-api/interfaces"
	"bobobox-api/model"
)

// ManageUsecase is
type ManageUsecase struct {
	ManageHotelRepository interfaces.ManageHotelRepository
}

// CreateManageHotelUsecase is
func CreateManageHotelUsecase(manageHotelUsecase interfaces.ManageHotelRepository) *ManageUsecase {
	return &ManageUsecase{
		ManageHotelRepository: manageHotelUsecase,
	}
}

// InsertHotelAndRoom is
func (b ManageUsecase) InsertHotelAndRoom(insrt model.Hotel) error {
	return b.ManageHotelRepository.InsertHotelAndRoom(insrt)
}

// InsertPriceBookingRoomHotel is
func (b ManageUsecase) InsertPriceBookingRoomHotel(prc []model.PricingRoom) error {
	return b.ManageHotelRepository.InsertPriceBookingRoomHotel(prc)
}

// DeleteRoomHotel is
func (b ManageUsecase) DeleteRoomHotel(del model.DeleteHotel) (err error) {
	return b.ManageHotelRepository.DeleteRoomHotel(del)
}

// DeleteHotel is
func (b ManageUsecase) DeleteHotel(id int) (err error) {
	return b.ManageHotelRepository.DeleteHotel(id)
}

// GetMasterData is
func (b ManageUsecase) GetMasterData(kategori string) (mstr []model.MasterData, err error) {
	return b.ManageHotelRepository.GetMasterData(kategori)
}

// UpdatePriceBookingRoomHotel is
func (b ManageUsecase) UpdatePriceBookingRoomHotel(prc model.PricingRoom) (err error) {
	return b.ManageHotelRepository.UpdatePriceBookingRoomHotel(prc)
}
