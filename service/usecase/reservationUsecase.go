package usecase

import (
	"bobobox-api/interfaces"
	"bobobox-api/model"
)

// ReservationUsecase is
type ReservationUsecase struct {
	ReservationRepository interfaces.ReservationRepository
}

// CreateReservationUsecase is
func CreateReservationUsecase(reservationUsecase interfaces.ReservationRepository) *ReservationUsecase {
	return &ReservationUsecase{
		ReservationRepository: reservationUsecase,
	}
}

// CheckAvailabllityRoomHotel is
func (b ReservationUsecase) CheckAvailabllityRoomHotel(check model.ChekingAvaibility) ([]model.AvaibiltyHotel, error) {
	return b.ReservationRepository.CheckAvailabllityRoomHotel(check)
}

// BookingRoomHotel is
func (b ReservationUsecase) BookingRoomHotel(order model.Reservation) error {
	return b.ReservationRepository.BookingRoomHotel(order)
}

// CheckinRoomHotel is
func (b ReservationUsecase) CheckinRoomHotel(stay model.Stay) error {
	return b.ReservationRepository.CheckinRoomHotel(stay)
}
