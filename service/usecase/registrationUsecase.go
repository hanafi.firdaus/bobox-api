package usecase

import (
	"bobobox-api/interfaces"
	"bobobox-api/model"
)

// RegistrationUsecase is
type RegistrationUsecase struct {
	RegistrationRepository interfaces.RegistrationRepository
}

// CreateRegistrationsUsecase is
func CreateRegistrationsUsecase(RegistrationUseCase interfaces.RegistrationRepository) *RegistrationUsecase {
	return &RegistrationUsecase{
		RegistrationRepository: RegistrationUseCase,
	}
}

// CreateUser is
func (b RegistrationUsecase) CreateUser(usr model.User) (model.User, error) {
	return b.RegistrationRepository.CreateUser(usr)
}

// GetUser is
func (b RegistrationUsecase) GetUser(phone string) (model.User, error) {
	return b.RegistrationRepository.GetUser(phone)
}
