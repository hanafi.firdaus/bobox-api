package repository

import (
	"bobobox-api/helper"
	"bobobox-api/model"
	"fmt"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// RegistrationRepository is
type RegistrationRepository struct {
	db *gorm.DB
}

// CreateRegistrationRepository is
func CreateRegistrationRepository(db *gorm.DB) *RegistrationRepository {
	return &RegistrationRepository{
		db,
	}
}

// CreateUser is
func (bio *RegistrationRepository) CreateUser(usr model.User) (us model.User, err error) {
	pass := ""
	err = bio.db.Where("phone = ?", usr.Phone).Find(&usr).Error
	if err != nil {
		if err.Error() == "record not found" {

			// generate random password
			pass = helper.RandomWords(4)

			// hasing password
			hash, errGenerateHashPass := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
			if errGenerateHashPass != nil {
				return us, errGenerateHashPass
			}

			usr.Password = string(hash)
			err = bio.db.Create(&usr).Error
			if err != nil {
				return
			}
		}
	} else {
		err = fmt.Errorf("This phone nomor has been register")
	}

	us = usr
	us.Password = pass
	return
}

// GetUser is
func (bio *RegistrationRepository) GetUser(phone string) (user model.User, err error) {
	err = bio.db.Where("phone = ?", phone).Find(&user).Error
	if err != nil {
		return
	}
	return
}
