package repository

import (
	"bobobox-api/model"
	"fmt"

	"github.com/jinzhu/gorm"
)

// ManageHotelRepository is
type ManageHotelRepository struct {
	db *gorm.DB
}

// CreateManageHotelRepository is
func CreateManageHotelRepository(db *gorm.DB) *ManageHotelRepository {
	return &ManageHotelRepository{
		db,
	}
}

// InsertHotelAndRoom is
func (r *ManageHotelRepository) InsertHotelAndRoom(insrt model.Hotel) (err error) {
	err = r.db.Create(&insrt).Error
	if err != nil {
		return
	}
	return
}

// InsertPriceBookingRoomHotel is
func (r *ManageHotelRepository) InsertPriceBookingRoomHotel(prc []model.PricingRoom) (err error) {
	tx := r.db.Begin()
	for _, element := range prc {
		err = tx.First(&model.Hotel{}, element.HotelID).Error
		if err != nil {
			if err.Error() == "record not found" {
				tx.Rollback()
				err = fmt.Errorf(fmt.Sprintf("Id hotel %d is not found", element.HotelID))
				return
			}
			return
		}

		err = tx.Create(&element).Error
		if err != nil {
			tx.Rollback()
			return
		}
	}

	tx.Commit()
	return
}

// DeleteRoomHotel is
func (r *ManageHotelRepository) DeleteRoomHotel(del model.DeleteHotel) (err error) {
	err = r.db.First(&model.Hotel{}, del.IDHotel).Error
	if err != nil {
		if err.Error() == "record not found" {
			err = fmt.Errorf(fmt.Sprintf("Id hotel %d is not found", del.IDHotel))
			return
		}
	}

	for _, element := range del.Room {
		err = r.db.Where("room_number = ? and hotel_id = ?", del.IDHotel, element).Delete(&model.Room{}).Error
		if err != nil {
			return
		}
	}

	return
}

// DeleteHotel is
func (r *ManageHotelRepository) DeleteHotel(id int) (err error) {
	err = r.db.First(&model.Hotel{}, id).Error
	if err != nil {
		if err.Error() == "record not found" {
			err = fmt.Errorf(fmt.Sprintf("Id hotel %d is not found", id))
			return
		}
	}

	var htl model.Hotel
	htl.ID = uint(id)
	err = r.db.Delete(&htl).Error
	if err != nil {
		return
	}
	return
}

// GetMasterData is
func (r *ManageHotelRepository) GetMasterData(kategori string) (mstr []model.MasterData, err error) {
	err = r.db.Raw("select * from master_data where kategori = '" + kategori + "'").Scan(&mstr).Error
	if err != nil {
		return
	}
	return
}

// UpdatePriceBookingRoomHotel is
func (r *ManageHotelRepository) UpdatePriceBookingRoomHotel(prc model.PricingRoom) (err error) {
	err = r.db.Model(&prc).Update("price", prc.Price).Error
	return
}
