package repository

// AvaibilityRoomHotel is
func AvaibilityRoomHotel(checkin, checkout string) string {
	return `select c.hotel_name as hotel, c.address, c.room_status as status, c.id as room_id, c.room_number, c.price, c.date
	from(
			select * from(
				select htl.hotel_name, htl.address, rm.room_status, rm.id, rm.room_number, pro.price, pro.date from hotel htl JOIN
				room rm on htl.id = rm.hotel_id join
				pricing_room pro on pro.room_number = rm.room_number
				where rm.room_status <> 'out of service' and pro.date >= '` + checkin + `' and pro.date <= '` + checkout + `'
				and rm.deleted_at is null and htl.deleted_at is null
			) as a
	) as c where c.id not in (
				select rm.id from hotel htl JOIN room rm on htl.id = rm.hotel_id join 
				reservation res on res.room_id = rm.id where res.checkin_date >= '` + checkin + `'
				and res.checkin_date < '` + checkout + `'
				OR res.checkin_date is null and rm.room_status <> 'out of service'
				AND res.order_id is not null 
				union 
				select rm.id from hotel htl JOIN room rm on htl.id = rm.hotel_id join 
				reservation res on res.room_id = rm.id where res.checkout_date >= '` + checkin + `'
				AND res.checkout_date >= '` + checkout + `'
				AND res.order_id is not null
	)`
}
