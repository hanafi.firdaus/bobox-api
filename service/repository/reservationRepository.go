package repository

import (
	"bobobox-api/model"

	"github.com/jinzhu/gorm"
)

// ReservationRepository is
type ReservationRepository struct {
	db *gorm.DB
}

// CreateReservationRepository is
func CreateReservationRepository(db *gorm.DB) *ReservationRepository {
	return &ReservationRepository{
		db,
	}
}

// CheckAvailabllityRoomHotel is
func (r *ReservationRepository) CheckAvailabllityRoomHotel(check model.ChekingAvaibility) (av []model.AvaibiltyHotel, err error) {
	query := AvaibilityRoomHotel(check.CheckinDate, check.CheckoutDate)
	err = r.db.Raw(query).Scan(&av).Error
	if err != nil {
		return
	}
	return
}

// BookingRoomHotel is
func (r *ReservationRepository) BookingRoomHotel(order model.Reservation) (err error) {
	err = r.db.Create(&order).Error
	if err != nil {
		return
	}
	return
}

// CheckinRoomHotel is
func (r *ReservationRepository) CheckinRoomHotel(stay model.Stay) (err error) {
	err = r.db.Create(&stay).Error
	if err != nil {
		return
	}
	return
}

// // InsertPriceRoomHotelRandom is
// func (r *ReservationRepository) InsertPriceRoomHotelRandom() (err error) {
// 	bulan := []string{"09", "10", "11", "12"}
// 	tgl := []string{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28"}
// 	htl := []int{1, 2, 3, 6}
// 	room := []int{1, 2, 3, 4}

// 	id := 0

// 	tx := r.db.Begin()
// 	var price model.PricingRoom
// 	for _, bln := range bulan {
// 		for _, tg := range tgl {
// 			for _, ht := range htl {
// 				for _, ro := range room {
// 					rand.Seed(time.Now().UnixNano())
// 					min := 210000
// 					max := 700000
// 					price.Date = "2020-" + bln + "-" + tg
// 					price.HotelID = ht
// 					price.RoomNumber = ro
// 					id = id + 1
// 					price.ID = uint(id)
// 					price.Price = float64(rand.Intn(max-min+1) + min)
// 					err = tx.Create(&price).Error
// 					if err != nil {
// 						tx.Rollback()
// 						return
// 					}
// 				}
// 			}
// 		}
// 	}

// 	tx.Commit()
// 	return
// }
