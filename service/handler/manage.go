package handler

import (
	"bobobox-api/helper"
	"bobobox-api/interfaces"
	"bobobox-api/model"
	"fmt"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

// CreateManageHotelHandler is
func CreateManageHotelHandler(usecase interfaces.ManageHotelUseCase) *Handler {
	return &Handler{
		ManageHotelUseCase: usecase,
	}
}

// InsertHotelAndRoom is
func (h *Handler) InsertHotelAndRoom(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	if claims["role"].(string) != "admin" {
		return helper.Errors(c, 401, fmt.Errorf("User role "+claims["role"].(string)+" is not allowed"))
	}

	var param = new(model.Hotel)
	if err := c.Bind(param); err != nil {
		return helper.Errors(c, 400, err)
	}

	err := h.ManageHotelUseCase.InsertHotelAndRoom(*param)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(201, nil)
}

// InsertPriceBookingRoomHotel is
func (h *Handler) InsertPriceBookingRoomHotel(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	if claims["role"].(string) != "admin" {
		return helper.Errors(c, 401, fmt.Errorf("User role "+claims["role"].(string)+" is not allowed"))
	}

	var param = new([]model.PricingRoom)
	if err := c.Bind(param); err != nil {
		return helper.Errors(c, 400, err)
	}

	err := h.ManageHotelUseCase.InsertPriceBookingRoomHotel(*param)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(201, nil)
}

// DeleteRoomHotel is
func (h *Handler) DeleteRoomHotel(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	if claims["role"].(string) != "admin" {
		return helper.Errors(c, 401, fmt.Errorf("User role "+claims["role"].(string)+" is not allowed"))
	}

	var param = new(model.DeleteHotel)
	if err := c.Bind(param); err != nil {
		return helper.Errors(c, 400, err)
	}

	err := h.ManageHotelUseCase.DeleteRoomHotel(*param)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(201, nil)
}

// DeleteHotel is
func (h *Handler) DeleteHotel(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	if claims["role"].(string) != "admin" {
		return helper.Errors(c, 401, fmt.Errorf("User role "+claims["role"].(string)+" is not allowed"))
	}

	var param = new(model.DeleteHotel)
	if err := c.Bind(param); err != nil {
		return helper.Errors(c, 400, err)
	}

	i, err := strconv.Atoi(c.QueryParam("id"))
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	err = h.ManageHotelUseCase.DeleteHotel(i)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(201, nil)
}

// GetMasterData is
func (h *Handler) GetMasterData(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	if claims["role"].(string) != "admin" {
		return helper.Errors(c, 401, fmt.Errorf("User role "+claims["role"].(string)+" is not allowed"))
	}

	kategori := ""
	if c.Param("name") == "room-status" {
		kategori = "room status"
	}

	mstr, err := h.ManageHotelUseCase.GetMasterData(kategori)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(200, mstr)
}

// UpdatePriceBookingRoomHotel is
func (h *Handler) UpdatePriceBookingRoomHotel(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	if claims["role"].(string) != "admin" {
		return helper.Errors(c, 401, fmt.Errorf("User role "+claims["role"].(string)+" is not allowed"))
	}

	var param = new(model.PricingRoom)
	if err := c.Bind(param); err != nil {
		return helper.Errors(c, 400, err)
	}

	err := h.ManageHotelUseCase.UpdatePriceBookingRoomHotel(*param)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(200, nil)
}
