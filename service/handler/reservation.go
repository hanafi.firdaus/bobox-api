package handler

import (
	"bobobox-api/helper"
	"bobobox-api/interfaces"
	"bobobox-api/model"
	"fmt"
	"math/rand"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

// CreateReservationHandler is
func CreateReservationHandler(usecase interfaces.ReservationUseCase) *Handler {
	return &Handler{
		ReservationUseCase: usecase,
	}
}

// CheckAvailabllityRoomHotel is
func (h *Handler) CheckAvailabllityRoomHotel(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	if claims["role"].(string) != "customer" {
		return helper.Errors(c, 401, fmt.Errorf("User role "+claims["role"].(string)+" is not allowed"))
	}

	ava, err := h.ReservationUseCase.CheckAvailabllityRoomHotel(model.ChekingAvaibility{
		CheckinDate:  c.QueryParam("checkin_date"),
		CheckoutDate: c.QueryParam("checkout_date"),
	})
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(200, ava)
}

// BookingRoomHotel is
func (h *Handler) BookingRoomHotel(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	if claims["role"].(string) != "customer" {
		return helper.Errors(c, 401, fmt.Errorf("User role "+claims["role"].(string)+" is not allowed"))
	}

	var param = new(model.Reservation)
	if err := c.Bind(param); err != nil {
		return helper.Errors(c, 400, err)
	}

	param.CustomerName = claims["name"].(string)

	rand.Seed(time.Now().UnixNano())
	param.OrderID = randomString(7)
	err := h.ReservationUseCase.BookingRoomHotel(*param)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(200, nil)
}

func randomString(len int) string {
	bytes := make([]byte, len)
	for i := 0; i < len; i++ {
		bytes[i] = byte(randomInt(65, 90))
	}
	return string(bytes)
}

// Returns an int >= min, < max
func randomInt(min, max int) int {
	return min + rand.Intn(max-min)
}

// CheckinRoomHotel is
func (h *Handler) CheckinRoomHotel(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	if claims["role"].(string) != "admin" {
		return helper.Errors(c, 401, fmt.Errorf("User role "+claims["role"].(string)+" is not allowed"))
	}

	var param = new(model.Stay)
	if err := c.Bind(param); err != nil {
		return helper.Errors(c, 400, err)
	}

	layoutISO := "2006-01-02"
	param.StayRoom.Date = time.Now().Format(layoutISO)
	param.StayRoom.RoomID = param.RoomID

	err := h.ReservationUseCase.CheckinRoomHotel(*param)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(200, nil)
}
