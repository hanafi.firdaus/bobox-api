package handler

import (
	"bobobox-api/helper"
	"bobobox-api/interfaces"
	"bobobox-api/model"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"golang.org/x/crypto/bcrypt"
)

type Handler struct {
	RegistrationUseCase interfaces.RegistrationUseCase
	ReservationUseCase  interfaces.ReservationUseCase
	ManageHotelUseCase  interfaces.ManageHotelUseCase
}

func CreateRegistrationHandler(usecase interfaces.RegistrationUseCase) *Handler {
	return &Handler{
		RegistrationUseCase: usecase,
	}
}

// CreateUser is
func (h *Handler) CreateUser(c echo.Context) error {
	var param = new(model.User)
	if err := c.Bind(param); err != nil {
		return helper.Errors(c, 400, err)
	}

	bio, err := h.RegistrationUseCase.CreateUser(*param)
	if err != nil {
		return helper.Errors(c, 400, err)
	}
	return c.JSON(200, bio)
}

// Login is
func (h Handler) Login(c echo.Context) error {

	var param = new(model.User)
	if err := c.Bind(param); err != nil {
		return helper.Errors(c, 400, err)
	}

	user, err := h.RegistrationUseCase.GetUser(param.Phone)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(param.Password))
	if err != nil {
		return helper.Errors(c, 401, err)
	}

	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = user.Name
	claims["phone"] = user.Phone
	claims["role"] = user.Role
	claims["timestamp"] = time.Now()

	tkn, err := token.SignedString([]byte(helper.Getenv("JWT_SECRET")))
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(200, map[string]string{
		"token": tkn,
	})
}
