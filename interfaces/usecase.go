package interfaces

import (
	"bobobox-api/model"
)

// RegistrationUseCase is
type RegistrationUseCase interface {
	CreateUser(usr model.User) (us model.User, err error)
	GetUser(phone string) (model.User, error)
}

// ReservationUseCase is
type ReservationUseCase interface {
	CheckAvailabllityRoomHotel(check model.ChekingAvaibility) (av []model.AvaibiltyHotel, err error)
	BookingRoomHotel(order model.Reservation) (err error)
	CheckinRoomHotel(stay model.Stay) (err error)
}

// ManageHotelUseCase is
type ManageHotelUseCase interface {
	InsertHotelAndRoom(insrt model.Hotel) error
	InsertPriceBookingRoomHotel(prc []model.PricingRoom) (err error)
	DeleteRoomHotel(del model.DeleteHotel) (err error)
	DeleteHotel(id int) (err error)
	GetMasterData(kategori string) (mstr []model.MasterData, err error)
	UpdatePriceBookingRoomHotel(prc model.PricingRoom) (err error)
}
