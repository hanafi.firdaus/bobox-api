package config

import (
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// Postgresql is
func Postgresql() *gorm.DB {
	connectionString := os.Getenv("GORM_CONNECTION")

	db, err := gorm.Open("postgres", connectionString)
	if err != nil {
		panic(err)
	}

	// db.CreateTable(&model.Reservation{})
	// db.CreateTable(&model.Stay{})
	// db.CreateTable(&model.StayRoom{})
	// db.CreateTable(&model.Hotel{})
	// db.CreateTable(&model.Room{})
	// db.CreateTable(&model.PricingRoom{})
	// db.CreateTable(&model.User{})
	// db.CreateTable(&model.MasterData{})

	return db
}
