package config

import (
	"github.com/go-redis/redis"
)

// Redis is
func Redis() (*redis.Client, error) {
	// better config nya di taroh di env
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	return client, nil

	// kayanya ini gak perlu
	// pong, err := config.redis.Ping().Result()
	// if err != nil {
	// 	return
	// }
	// fmt.Println(pong)
	// return
}
